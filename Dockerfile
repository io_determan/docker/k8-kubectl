FROM debian:buster as builder

ARG BASE_VERSION

ENV KUBE_VERSION=v$BASE_VERSION

WORKDIR /root/

RUN apt-get update \
 && apt-get install -y wget 

RUN wget https://dl.k8s.io/release/${KUBE_VERSION}/bin/linux/amd64/kubectl \
 && chmod +x kubectl

COPY docker-entrypoint.sh /root/docker-entrypoint.sh 
RUN chmod +x /root/docker-entrypoint.sh 

FROM debian:buster-slim

COPY --from=builder /root/kubectl /usr/local/bin
COPY --from=builder /root/docker-entrypoint.sh  /usr/local/bin

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["kubectl"]
