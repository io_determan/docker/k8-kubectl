#!/usr/bin/env bash
set -e

mkdir -p "$CI_PROJECT_DIR.tmp"

if [ -z "$KUBE_URL" ] || [ -z "$KUBE_TOKEN" ] || [ -z "$KUBE_CA_PEM_FILE" ]; then
  echo 'KUBE_URL, KUBE_TOKEN, and/or KUBE_CA_PEM_FILE are unset. Auto kubectl config disabled.'        
else
  CA_PEM_FILE="$CI_PROJECT_DIR.tmp/CA_PEM_FILE"
  if [[ -f "$KUBE_CA_PEM_FILE" ]]; then
      cat "$KUBE_CA_PEM_FILE" > "$CA_PEM_FILE"
  else
      echo "$KUBE_CA_PEM_FILE" > "$CA_PEM_FILE"
  fi

  kubectl config set-credentials kubeuser/kubernetes --token="$KUBE_TOKEN"
  kubectl config set-cluster kubernetes --server="$KUBE_URL" --certificate-authority="$CA_PEM_FILE" --embed-certs=true
  kubectl config set-context default/kubernetes/kubeuser --user=kubeuser/kubernetes --namespace="$IO_HELM_NAMESPACE" --cluster=kubernetes
  kubectl config use-context default/kubernetes/kubeuser
fi

exec "$@"
